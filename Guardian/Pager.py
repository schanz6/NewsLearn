import sys


class Pager:

    apiHelper = None
    searchTerm = ''
    pageSize = 50
    currentPage = 1
    numberOfPages = -1
    isOldest = True
    fromDate = None
    
    def __init__(self, apiHelper, searchTerm, isOldest, pageSize,fromDate):
        self.apiHelper = apiHelper
        self.pageSize = pageSize
        self.searchTerm = searchTerm
        self.isOldest = isOldest
        self.fromDate = fromDate

    def Next(self):
        
        if(self.HasNext()):
            item = self.apiHelper.Search(self.searchTerm, self.isOldest, self.currentPage, self.fromDate)
            self.currentPage = self.currentPage+1
            response = item["response"]
            
            if(self.numberOfPages == -1):
                self.numberOfPages = response["pages"]

            return response["results"]
        else:
            return None


    def HasNext(self):
        return self.numberOfPages == -1 or self.currentPage <= self.numberOfPages

    def CurrentPage(self):
        return self.currentPage 

    def NumberOfPages(self):
        if(self.numberOfPages == -1):
            return None
        else :
            return self.numberOfPages 


