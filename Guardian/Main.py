import sys
from collections import namedtuple
import GuardianApiHelper
import Utility
import Pager
import pika
import json

sys.path.insert(0, '..')
import config


def main():
	queueName = "guardian"
	connection = pika.BlockingConnection(pika.ConnectionParameters(host=config.Rabit_Host))
	channel = connection.channel()

	channel.queue_declare(queue=queueName)
	

	
	def callback(ch, method, properties, body):
		Query = json.loads(body)
		print(Query)
		
		gardian = GuardianApiHelper.GuardianApiHelper(config.Guardian_Key)
		pager = Pager.Pager(gardian, Query['Search'], Query['Order'], 50, Query["FromDate"])

		
		while pager.HasNext():
			Result = {}
			Result['SearchHeader'] = Query
			Result['PageNumber'] = pager.CurrentPage()

			Result['SentQueue'] = queueName
			page = pager.Next()
			

			#Needs to be done after first get
			Result['TotalPages'] = pager.NumberOfPages()
			
			if page is None:
				Result['Error'] = True
				Result['Result'] = None
			else :
				Result['Error'] = False
				Result['Result'] = json.dumps(Utility.Clean(page))


			channel.basic_publish(exchange='',
	                      routing_key=Query['ReturnQueue'],
	                      body=json.dumps(Result))



	channel.basic_consume(callback,
                      queue=queueName,
                      no_ack=True)

	
	channel.start_consuming()

def test():
	gardian = GuardianApiHelper.GuardianApiHelper(config.Guardian_Key)
	pager = Pager.Pager(gardian, 'tomhanks', True, 10, '2000-01-03')
	page = pager.Next()

	print(json.dumps(Utility.Clean(page)))


if __name__ == '__main__':
    #main()
    test()
