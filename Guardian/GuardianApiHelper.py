import sys
import requests


class GuardianApiHelper:
    
    key = None
    baseUrl = 'https://content.guardianapis.com'
    keyPeram = 'api-key'
    
    def __init__(self, key):
        self.key = key

    def GetRequest(self, url):
        requestUrl = self.baseUrl+url

        if "?" in url:
            requestUrl = requestUrl + "&"+ self.keyPeram + '=' + self.key
        else:
            requestUrl = requestUrl + "?"+ self.keyPeram + '=' + self.key

        print(requestUrl)
        
        r = requests.get(requestUrl)
        return r.json()

    def Search(self, searchTerm, isOldest=True, page=1, fromDate='1890-01-01'):
        order = 'newest'
        print(fromDate)
        if isOldest == True:
            order = 'oldest'

        query = '/search?show-fields=headline,body,lastModified&page-size=50&order-by='+order+'&q='+ searchTerm +'&page=' + str(page) +'&from-date='+fromDate
        return self.GetRequest(query)

