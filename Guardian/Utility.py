import re
import json

def CleanHtml(raw_html):
	cleanr = re.compile('<.*?>')
	cleantext = re.sub(cleanr, '', raw_html)
	return cleantext

def Clean(result):
	for item in result:
		item["date"] = item["webPublicationDate"]
		item["headline"] = CleanHtml(item["fields"]["headline"])
		item["body"] = CleanHtml(item["fields"]["body"])
		del item["isHosted"]
		del item["fields"]
		del item["webTitle"]
		del item["apiUrl"]
		if "pillarName" in item.keys():
			del item["pillarName"]
		del item["sectionName"]
		del item["webUrl"]
		del item["webPublicationDate"]



	return result

