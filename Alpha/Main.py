import sys
from collections import namedtuple
import AlphaApiHelper
import Utility
import pika
import json

sys.path.insert(0, '..')
import config

def test():
	alpha = AlphaApiHelper.AlphaApiHelper(config.Alpha_Key)
		
	Result = alpha.Search('MSFT')

	
	print(json.dumps(Utility.Clean(Result)))
	#print(Result["Time Series (Daily)"]["2017-11-16"])


def main():
	queueName = "alpha"
	connection = pika.BlockingConnection(pika.ConnectionParameters(host=config.Rabit_Host))
	channel = connection.channel()

	channel.queue_declare(queue=queueName)
	

	# body = ReturnQueue Search Order 
	def callback(ch, method, properties, body):
		Query = json.loads(body)
		print(Query)
		
		alpha = AlphaApiHelper.AlphaApiHelper(config.Alpha_Key)
		
		Result = alpha.Search(Query['Search'])

		Result = Utility.Clean(Result)
		channel.basic_publish(exchange='',
                      routing_key=Query['ReturnQueue'],
                      body=json.dumps(Result))



	channel.basic_consume(callback,
                      queue=queueName,
                      no_ack=True)

	
	channel.start_consuming()


if __name__ == '__main__':
    test()
