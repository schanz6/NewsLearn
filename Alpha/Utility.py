import re
import json
import datetime 

def CleanHtml(raw_html):
	clean = re.compile('<.*?>')
	cleantext = re.sub(clean, '', raw_html)
	return cleantext

def Clean(result):

	result["Data"] = result["Time Series (Daily)"]
	del result["Time Series (Daily)"]

	LastDate = datetime.datetime.now()
	FirstDate = datetime.datetime(1, 1, 1)

	for key, value in result["Data"].items(): 
		
		KeyDate = datetime.datetime.strptime(key, "%Y-%m-%d")

		if KeyDate > FirstDate:
			FirstDate = KeyDate

		if KeyDate < LastDate:
			LastDate = KeyDate
		#LastDate = key
		value["open"] = value["1. open"] 
		value["high"] = value["2. high"] 
		value["low"] = value["3. low"] 
		value["close"] = value["4. close"] 
		value["adjusted close"] = value["5. adjusted close"] 
		value["volume"] = value["6. volume"] 
		value["dividend amount"] = value["7. dividend amount"] 
		value["split coefficient"] = value["8. split coefficient"] 

		del value["1. open"] 
		del value["2. high"] 
		del value["3. low"] 
		del value["4. close"] 
		del value["5. adjusted close"] 
		del value["6. volume"] 
		del value["7. dividend amount"] 
		del value["8. split coefficient"] 


	result["Meta Data"]["Information"] = result["Meta Data"]["1. Information"]
	result["Meta Data"]["Symbol"] = result["Meta Data"]["2. Symbol"]
	result["Meta Data"]["Last Refreshed"] = result["Meta Data"]["3. Last Refreshed"]
	result["Meta Data"]["Output Size"] = result["Meta Data"]["4. Output Size"]
	result["Meta Data"]["Time Zone"] = result["Meta Data"]["5. Time Zone"]
	result["Meta Data"]["Oldest Date"] = LastDate.strftime("%Y-%m-%d")
	result["Meta Data"]["Newest Date"] = FirstDate.strftime("%Y-%m-%d")

	del result["Meta Data"]["1. Information"] 
	del result["Meta Data"]["2. Symbol"]
	del result["Meta Data"]["3. Last Refreshed"] 
	del result["Meta Data"]["4. Output Size"] 
	del result["Meta Data"]["5. Time Zone"] 



	return result
