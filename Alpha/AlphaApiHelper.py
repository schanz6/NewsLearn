import sys
import requests

class AlphaApiHelper:
  
    key = None
    baseUrl = 'https://www.alphavantage.co/query'
    keyPeram = 'apikey'
    
    def __init__(self, key):
        self.key = key

    def GetRequest(self, url):
        requestUrl = self.baseUrl+url

        if "?" in url:
            requestUrl = requestUrl + "&"+ self.keyPeram + '=' + self.key
        else:
            requestUrl = requestUrl + "?"+ self.keyPeram + '=' + self.key

        r = requests.get(requestUrl)
        print(r)
        return r.json()

    def Search(self, searchTerm):
        query = self.baseUrl+'?function=TIME_SERIES_DAILY_ADJUSTED&symbol='+ searchTerm
        return self.GetRequest(query)

